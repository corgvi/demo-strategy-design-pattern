package org.example.strategyparttern;

import org.example.model.Part;

public class Context {
    private PartStrategy partStrategy;

    public Context(PartStrategy partStrategy) {
        this.partStrategy = partStrategy;
    }

    public Part excutePartStrategy(Part part){
        return partStrategy.doStrategy(part);
    }
}
