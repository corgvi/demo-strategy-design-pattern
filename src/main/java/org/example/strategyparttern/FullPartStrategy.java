package org.example.strategyparttern;

import org.example.model.Part;

public class FullPartStrategy implements PartStrategy{
    @Override
    public Part doStrategy(Part part) {
        return part;
    }
}
