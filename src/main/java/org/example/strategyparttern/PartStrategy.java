package org.example.strategyparttern;

import org.example.model.Part;

public interface PartStrategy {
    public Part doStrategy(Part part);
}
