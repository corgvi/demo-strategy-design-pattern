package org.example.model;

public class Part {
    private Integer id;

    private boolean enabled;

    private String caution;

    private String comment;

    private String iconPath;

    private Material material;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getCaution() {
        return caution;
    }

    public void setCaution(String caution) {
        this.caution = caution;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Part(Integer id, boolean enabled, String caution, String comment, String iconPath, Material material) {
        this.id = id;
        this.enabled = enabled;
        this.caution = caution;
        this.comment = comment;
        this.iconPath = iconPath;
        this.material = material;
    }

    @Override
    public String toString() {
        return "Part{" +
                "id=" + id +
                ", enabled=" + enabled +
                ", caution='" + caution + '\'' +
                ", comment='" + comment + '\'' +
                ", iconPath='" + iconPath + '\'' +
                ", material=" + material +
                '}';
    }
}
