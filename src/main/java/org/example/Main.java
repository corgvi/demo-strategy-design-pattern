package org.example;

import org.example.model.Material;
import org.example.model.Part;
import org.example.strategyparttern.Context;
import org.example.strategyparttern.FullPartStrategy;
import org.example.strategyparttern.PlassticPartStrategy;
import org.example.strategyparttern.PreTreatmentPartStrategy;

public class Main {
    public static void main(String[] args) {
        Part part1 = new Part(1, true, "caution 1", "comment 1", "icon path 1", new Material(1, "Copper"));
        Part part2 = new Part(2, false, "caution 2", "comment 2", "icon path 2", new Material(2, "Steel"));
        Part part3 = new Part(3, true, "caution 3", "comment 3", "icon path 3", new Material(3, "Plastic"));
        Context context = new Context(new PreTreatmentPartStrategy());
        System.out.println(context.excutePartStrategy(part1));

        context = new Context(new FullPartStrategy());
        System.out.println(context.excutePartStrategy(part2));

        context = new Context(new PlassticPartStrategy());
        System.out.println(context.excutePartStrategy(part3));
    }
}